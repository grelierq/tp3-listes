class Liste{
    private int val;
    private Liste suiv;
    
    //attention : utilisez plutôt "getVal()" que "val", car getVal() renvoie une exception quand la liste est vide,
	//et vous empêche donc d'utiliser la valeur "fantôme" du dernier maillon

    //liste vide =_def (*,null)
 
    //////////////////////////////////////////////
    //////// méthodes fournies
    //////////////////////////////////////////////
    
    public Liste(){
	suiv = null;
    }

    public Liste(Liste l){
	if(l.estVide()){
	    suiv=null;
	}
	else{
	    val = l.val;
	    suiv = new Liste(l.suiv);
	}
    }

    public Liste(int x, Liste l){
	val = x;
	suiv = new Liste(l);
    }

	public int getVal(){
		//sur liste non vide
		if(estVide())
			throw new RuntimeException("getVal appelée sur liste vide");
		return val;
	}
	public Liste getSuiv(){
		return suiv;
	}

	public void ajoutTete(int x){
		if(estVide()){
			val = x;
			suiv = new Liste();
		}
		else {
			Liste aux = new Liste();
			aux.val = getVal();
			aux.suiv = suiv;
			val = x;
			suiv = aux;
		}
	}

    public void supprimeTete(){
		//sur liste non vide
		if(suiv.estVide()){
			suiv = null;
		}
		else {
			val = suiv.getVal();
			suiv = suiv.suiv;
		}
    }

    public boolean estVide(){
	return suiv==null;
    }



    public String toString(){
	if(estVide()){
	    return "()";
	}
	else{
	    return getVal()+" "+suiv.toString();
	}
    }



    //////////////////////////////////////////////
    //////// méthodes du TD
    //////////////////////////////////////////////
    
    

    public static void main(String[] arg){
	Liste l = new Liste();
	l.ajoutTete(4);
	l.ajoutTete(3);
	l.ajoutTete(2);
	System.out.println("l : " + l);
	l.supprimeTete();
	System.out.println("l : " + l);
	l.supprimeTete();
	System.out.println("l : " + l);
	l.supprimeTete();
	System.out.println("l : " + l);


	}
}
